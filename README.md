# How the back ups run
The code to run the backups uses a simple tool called elasticdump or elasticsearch-dump. See https://www.npmjs.com/package/elasticdump

This is a nodejs tool. It is run by a static version of the current (Feb 2017) NodeLTS version which can be found in the home directory.

We use cron to run `jlocal runbackup.sh` . We use jlocal because the bash script includes a jsub command to do the actualy backup and delete on the grid. You can't seem to call jsub from jsub
This creates a folder in esbackups with todays date in ISO format.
Submits a job to the grid. N.B. 6Gig ram. It does odd things if it runs out of memory.
Then delete the job from 10 days ago

# How to restore from backups
You can restore one index at a time using elasticdump and follow the instructions at https://www.npmjs.com/package/elasticdump with an input of the backup on disk and an output of the index on the cluster.

