#!/bin/bash

mkdir -p ~/esbackup/$(date -I)
jsub -mem 6G ~/node-v6.9.5-linux-x64/bin/node ./node_modules/elasticdump/bin/multielasticdump --input="http://tools-elastic-01.tools.eqiad.wmflabs:80" --output=esbackups/$(date -I) &&
jsub rm -rf ~/esbackups/$(date -I -d "-10 days")
